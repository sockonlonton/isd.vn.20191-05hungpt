package Ticket;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.gate.Gate;
import hust.soict.se.scanner.CardScanner;

public class Test {
    
    public static void main (String argv[]) {
    	String pseudoBarCode = "ABCDEFGH";
        CardScanner cardScanner = CardScanner.getInstance();
        try {
			String cardId = cardScanner.process(pseudoBarCode);
	    	System.out.println(cardId);
		} catch (InvalidIDException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        Gate newGate = Gate.getInstance();
        
        newGate.open();
        
    }
}
