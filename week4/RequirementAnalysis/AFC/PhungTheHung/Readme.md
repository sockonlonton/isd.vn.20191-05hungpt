## The work I have done:

### Finish requirement analysis for the Automated Fare Controller (AFC) system
-   Update last week homework (use case) based on comments and discussion on class
-   Glossary: Define some common terminologies in the system.
-   Do the specification for all use case (equally divided among the members)
-   Complete the SRS
- Design my Usecase Diagram for AFC
- Dicuss with other member for team's Usecase Diagram
- Analysis the flow of event for these following usecase: 
	+ Exit the platform area
	+ Scan  Prepaid Card