## The work we have done
- Create folder Architecture Design
- Design sequence diagram
	+ Tran Thi Thanh Huyen: Enter the platform by one-way ticket
			        Enter the platform by 24 hours ticket
			        Enter the platform by prepaid card
	+ Phung The Hung: Exit the platform
			  Scan prepaid card
	+ Nguyen Viet Hung: Control status gate
			    Charge more money
