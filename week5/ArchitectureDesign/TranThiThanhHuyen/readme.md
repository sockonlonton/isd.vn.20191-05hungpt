## The work I have done:
- Edit usecase diagram, sequence diagram for usecase:
	+ Enter by 1-way ticket.
	+ Enter by 24h ticket.
- Create communication diagram and analysis diagram for:
	+ Enter by 1-way ticket.
	+ Enter by 24h ticket.
- Create combined class diagram for system with my teammate.
