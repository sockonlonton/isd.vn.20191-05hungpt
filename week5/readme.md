## The work we have done
- Edit usecase diagram and sequence diagram.
- Create communication diagram and analysis class diagram:
	+ Tran Thi Thanh Huyen: Enter by 1-way ticket, Enter by 24h ticket.
	+ Phung The Hung: Enter by prepaid card, Exit by prepaid card.
	+ Nguyen Viet Hung: Exit by 1-way ticket, Exit by 24h ticket.
- Create combined class diagram for system.
